#!/bin/bash

if [ "$1" == "" ]; then
	echo "Error: You did not specify the path of the directory to process"
	echo "Syntax: $0 /path/to/project/lib/containing/jar/files"
	exit 1;
fi

cd "$1"

for file in `find -name "*.jar" | sed "s/^\.\///g"`; do 

VERSION=`unzip -p - $file META-INF/maven/*/*/pom.properties 2>/dev/null | grep "^version" | cut -d '=' -f 2 | sed -e 's/[[:space:]]*$//'`
ART=`unzip -p - $file META-INF/maven/*/*/pom.properties 2>/dev/null | grep "^artifactId" | cut -d '=' -f 2 | sed -e 's/[[:space:]]*$//'`
GROUP=`unzip -p - $file META-INF/maven/*/*/pom.properties 2>/dev/null | grep "^groupId" | cut -d '=' -f 2 | sed -e 's/[[:space:]]*$//'`

echo "VERSION--------->$VERSION"
echo "ART--------->$ART"
echo "GROUP--------->$GROUP"
echo "file--------->$file"
echo "------------------------------------------>"

#If I am able to extract GROUP, then i am fine.
if [ "$GROUP" != "" ]; then
	echo "$file found dep info in jar"
	echo "<dependency>" >> pom.xml
	echo "    <groupId>$GROUP</groupId>" >> pom.xml
	echo "    <artifactId>$ART</artifactId>" >> pom.xml
	echo "    <version>$VERSION</version>" >> pom.xml
	echo "</dependency>" >> pom.xml
	echo "" >> pom.xml
else
	SHA1=`sha1sum $file`
	#LOOKUPINFO=`lookup-jar.py $file $SHA1`
	echo "SHA1--------->$SHA1"
	
# call python script to lookup jar by SHA1 checksum on search.maven.org
	LOOKUPINFO=$(python - $file $SHA1 << END
import json
from urllib.request import urlopen
import sys
import os
jar = sys.argv[1]
sha = sys.argv[2]



searchurl = 'http://search.maven.org/solrsearch/select?q=1:%22'+sha+'%22&rows=20&wt=xml'
print('searchurl-->',searchurl)
page = urlopen(searchurl)
print('page-->',page)
readline = page.readlines()
print('readline==>',readline)
END
)

echo "LOOKUPINFO--------->$LOOKUPINFO"
echo $LOOKUPINFO >> repo.txt

fi

done
